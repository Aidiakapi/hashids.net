![hashids](http://www.hashids.org.s3.amazonaws.com/public/img/hashids.png "Hashids")

======

What is it?
-------

A small .NET class to generate YouTube-like hashes from one or many numbers. Use hashids when you do not want to expose your database ids to the user. Learn all the details about it at: [http://www.hashids.org/](http://www.hashids.org/)


Usage
-------

#### Encrypting one number

You can pass a unique salt value so your hashes differ from everyone else's. I use "this is my salt" as an example.


var hashids = new HashIds("this is my salt");
string hash = hashids.Encrypt(12345);

`hash` is now going to be:
	
	NkK9

#### Decrypting

Notice during decryption, same salt value is used:

	::c#
	var hashids = new HashIds("this is my salt");
	int[] numbers = hashids.Decrypt("NkK9");

`numbers` is now going to be:
	
	new int[] { 12345 }

#### Decrypting with different salt

Decryption will not work if salt is changed:

	::c#
	var hashids = new HashIds("this is my pepper");
	int[] numbers = hashids.Decrypt("NkK9");

`numbers` is now going to be:
	
	new int[] { }
	
#### Encrypting several numbers

	::c#
	var hashids = new HashIds("this is my salt");
	string hash = hashids.Encrypt(683, 94108, 123, 5);

`hash` is now going to be:
	
	aBMswoO2UB3Sj

#### Decrypting is done the same way

	::c#
	var hashids = new HashIds("this is my salt");
	int[] numbers = hashids.Decrypt("aBMswoO2UB3Sj");

`numbers` is now going to be:
	
	new int[] { 683, 94108, 123, 5 }
	
#### Encrypting and specifying minimum hash length

Here we encrypt integer 1, and set the **minimum** hash length to **8** (by default it's **0** -- meaning hashes will be the shortest possible length).

	::c#
	var hashids = new HashIds("this is my salt", 8);
	string hash = hashids.Encrypt(1);

`hash` is now going to be:
	
	gB0NV05e
	
#### Decrypting

	::c#
	var hashids = new HashIds("this is my salt", 8);
	int[] numbers = hashids.Decrypt("gB0NV05e");

`numbers` is now going to be:
	
	new int[] { 1 }
	
#### Specifying custom hash alphabet

Here we set the alphabet to consist of valid hex characters: "0123456789abcdef"

	::c#
	// You can use named parameters, or set the minimum hash length to 0
	var hashids = new HashIds(salt: "this is my salt", alphabet: "0123456789abcdef");
	string hash = hashids.Encrypt(1234567);

`hash` is now going to be:
	
	b332db5


License
-------

MIT License. See the `LICENSE` file. You can use HashIds.Net in open source projects and commercial products.