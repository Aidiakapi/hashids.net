﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Hashids.Net.Tests
{
    [TestClass]
    public class HashIdsTest
    {
        [TestMethod]
        public void HashIds_EncryptOneNumber()
        {
            var hashids = new HashIds("this is my salt");
            string hash = hashids.Encrypt(12345);
            Assert.AreEqual("NkK9", hash);
        }

        [TestMethod]
        public void HashIds_DecryptOneNumber()
        {
            var hashids = new HashIds("this is my salt");
            int[] numbers = hashids.Decrypt("NkK9");
            CollectionAssert.AreEqual(new[] { 12345 }, numbers);
        }

        [TestMethod]
        public void HashIds_DecryptDifferentSalt()
        {
            var hashids = new HashIds("this is my pepper");
            int[] numbers = hashids.Decrypt("NkK9");
            CollectionAssert.AreEqual(new int[0], numbers);
        }

        [TestMethod]
        public void HashIds_EncryptSeveralNumbers()
        {
            var hashids = new HashIds("this is my salt");
            string hash = hashids.Encrypt(683, 94108, 123, 5);
            Assert.AreEqual("aBMswoO2UB3Sj", hash);
            hash = hashids.Encrypt(new[] { 683, 94108, 123, 5 });
            Assert.AreEqual("aBMswoO2UB3Sj", hash);
        }

        [TestMethod]
        public void HashIds_DecryptSeveralNumbers()
        {
            var hashids = new HashIds("this is my salt");
            int[] numbers = hashids.Decrypt("aBMswoO2UB3Sj");
            CollectionAssert.AreEqual(new[] { 683, 94108, 123, 5 }, numbers);
        }

        [TestMethod]
        public void HashIds_EncryptMinimumLength()
        {
            var hashids = new HashIds("this is my salt", 8);
            string hash = hashids.Encrypt(1);
            Assert.AreEqual("gB0NV05e", hash);
        }

        [TestMethod]
        public void HashIds_DecryptMinimumLength()
        {
            var hashids = new HashIds("this is my salt", 8);
            int[] numbers = hashids.Decrypt("gB0NV05e");
            CollectionAssert.AreEqual(new[] { 1 }, numbers);
        }

        [TestMethod]
        public void HashIds_EncryptCustomAlphabet()
        {
            var hashids = new HashIds(salt: "this is my salt", alphabet: "0123456789abcdef");
            string hash = hashids.Encrypt(1234567);
            Assert.AreEqual("b332db5", hash);
        }

        [TestMethod]
        public void HashIds_DecryptCustomAlphabet()
        {
            var hashids = new HashIds(salt: "this is my salt", alphabet: "0123456789abcdef");
            int[] numbers = hashids.Decrypt("b332db5");
            CollectionAssert.AreEqual(new[] { 1234567 }, numbers);
        }

        [TestMethod]
        public void HashIds_EncryptOneMaxNumber()
        {
            var hashids = new HashIds("this is my salt");
            string hash = hashids.Encrypt(int.MaxValue);
            Assert.AreEqual("ykJWW1g", hash);
        }

        [TestMethod]
        public void HashIds_DecryptOneMaxNumber()
        {
            var hashids = new HashIds("this is my salt");
            int[] numbers = hashids.Decrypt("ykJWW1g");
            CollectionAssert.AreEqual(new[] { int.MaxValue }, numbers);
        }

        [TestMethod]
        public void HashIds_EncryptSeveralMaxNumbers()
        {
            var hashids = new HashIds("this is my salt");
            string hash = hashids.Encrypt(int.MaxValue, int.MaxValue, int.MaxValue);
            Assert.AreEqual("81wxx3ZHPe114jC65PP3g", hash);
        }

        [TestMethod]
        public void HashIds_DecryptSeveralMaxNumbers()
        {
            var hashids = new HashIds("this is my salt");
            int[] numbers = hashids.Decrypt("81wxx3ZHPe114jC65PP3g");
            CollectionAssert.AreEqual(new[] { int.MaxValue, int.MaxValue, int.MaxValue }, numbers);
        }

        [TestMethod]
        public void HashIds_EncryptAndDecryptRandom()
        {
            var hashids = new HashIds("this is my salt");
            for (int iter = 0; iter < 1000; iter++)
            {
                var rnd = new Random(iter);
                // 1 - 10 numbers
                var numbers = new int[rnd.Next(1, 11)];
                for (int i = 0; i < numbers.Length; i++)
                    // 0 <= rnd <= int.MaxValue
                    numbers[i] = rnd.Next(-1, int.MaxValue) + 1;

                string hash = hashids.Encrypt(numbers);
                int[] decryptedNumbers = hashids.Decrypt(hash);
                CollectionAssert.AreEqual(numbers, decryptedNumbers, "iter: {0}", iter);
            }
        }
    }
}
