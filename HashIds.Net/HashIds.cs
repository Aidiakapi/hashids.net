﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Hashids.Net
{
    /// <summary>
    /// Generate short hashes from numbers (like YouTube and Bitly).
    /// <para>
    /// Hashids was designed for use in URL shortening, tracking stuff,
    /// validating accounts and making pages private (through abstraction).
    /// Instead of showing items as <c>1</c>, <c>2</c>, or <c>3</c>,
    /// you could show them as <c>b9iLXiAa</c>, <c>EATedTBy</c>, and <c>Aaco9cy5</c>.
    /// Hashes depend on your salt value.
    /// </para>
    /// </summary>
    /// <threadsafety instance="true" static="true" />
    public class HashIds
    {
        // Alphabet info
        private const string DEFAULT_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        private const string DEFAULT_SEPS = "cfhistuCFHISTU";

        // Error messages
        private const string ERROR_ALPHABET_LENGTH = "alphabet must contain at least X unique characters";
        private const string ERROR_ALPHABET_SPACE = "alphabet cannot contain spaces";

        // Internal settings
        private const int MIN_ALPHABET_LENGTH = 16;
        private const double SEP_DIV = 3.5;
        private const double GUARD_DIV = 12;

        private readonly string _salt;
        private readonly int _minHashLength;

        private readonly string _alphabet;
        private readonly string _seps;
        private readonly string _guards;

        /// <summary>
        /// Creates a hasher with a specific salt, minimum hash length and alphabet.
        /// </summary>
        /// <param name="salt">Salt to hash with.</param>
        /// <param name="minHashLength">Minimum hash length.</param>
        /// <param name="alphabet">Specific alphabet.</param>
        /// <exception cref="ArgumentException">Thrown when the supplied alphabet is too short or contains a space.</exception>
        public HashIds(string salt = null, int minHashLength = 0, string alphabet = null)
        {
            if (salt == null) salt = string.Empty;
            if (minHashLength < 0) minHashLength = 0;

            // Get the unique alphabet
            alphabet = alphabet == null
                ? DEFAULT_ALPHABET
                : getUniqueAlphabet(alphabet);

            if (alphabet.Length < MIN_ALPHABET_LENGTH)
                throw new ArgumentException(ERROR_ALPHABET_LENGTH.Replace("X", MIN_ALPHABET_LENGTH.ToString(CultureInfo.InvariantCulture)), "alphabet");

            if (alphabet.Contains(" "))
                throw new ArgumentException(ERROR_ALPHABET_SPACE, "alphabet");

            _seps = DEFAULT_SEPS;
            setUpAlphabet(salt, ref alphabet, ref _seps, out _guards);

            _salt = salt;
            _alphabet = alphabet;
            _minHashLength = minHashLength;
        }

        private static void setUpAlphabet(string salt, ref string alphabet, ref string seps, out string guards)
        {
            // Seps should contain only characters present in alphabet; alphabet should not contains seps
            for (int i = 0, len = seps.Length; i != len; i++)
            {
                int j = alphabet.IndexOf(seps[i]);
                if (j == -1) seps = seps.Substring(0, i) + " " + seps.Substring(i + 1);
                else alphabet = alphabet.Substring(0, j) + " " + alphabet.Substring(j + 1);
            }

            alphabet = alphabet.Replace(" ", "");
            seps = consistentShuffle(seps.Replace(" ", ""), salt);

            if (seps.Length == 0 || ((double)alphabet.Length / seps.Length) > SEP_DIV)
            {
                var sepsLength = (int)Math.Ceiling(alphabet.Length / SEP_DIV);
                if (sepsLength == 1) sepsLength++;

                if (sepsLength > seps.Length)
                {
                    int diff = sepsLength - seps.Length;
                    seps += alphabet.Substring(0, diff);
                    alphabet = alphabet.Substring(diff);
                }
                else seps = seps.Substring(0, sepsLength);
            }

            alphabet = consistentShuffle(alphabet, salt);
            var guardCount = (int)Math.Ceiling(alphabet.Length / GUARD_DIV);

            if (alphabet.Length < 3)
            {
                guards = seps.Substring(0, guardCount);
                seps = seps.Substring(guardCount);
            }
            else
            {
                guards = alphabet.Substring(0, guardCount);
                alphabet = alphabet.Substring(guardCount);
            }
        }

        private static string consistentShuffle(string alphabet, string salt)
        {
            int i, v, p;
            if (salt.Length == 0) return alphabet;

            for (i = alphabet.Length - 1, v = 0, p = 0; i > 0; i--, v++)
            {
                v %= salt.Length;
                int integer;
                p += integer = salt[v];
                int j = (integer + v + p) % i;
                char temp = alphabet[j];
                alphabet = alphabet.Substring(0, j) + alphabet[i] + alphabet.Substring(j + 1);
                alphabet = alphabet.Substring(0, i) + temp + alphabet.Substring(i + 1);
            }
            return alphabet;
        }

        private static string hash(int input, string alphabet)
        {
            var hash = "";
            int alphabetLength = alphabet.Length;

            do
            {
                hash = alphabet[input % alphabetLength] + hash;
                input = (int)((double)input / alphabetLength);
            }
            while (input > 0);

            return hash;
        }

        private static string getUniqueAlphabet(string alphabet)
        {
            var uniqueCheck = new HashSet<char>();
            var buffer = new char[alphabet.Length];
            int bufferCount = 0;

            foreach (char character in alphabet)
            {
                if (uniqueCheck.Add(character))
                    buffer[bufferCount++] = character;
            }

            // If the buffer contains the full length, there were no duplicates.
            return bufferCount == alphabet.Length
                ? alphabet
                : new string(buffer, 0, bufferCount);
        }

        /// <summary>
        /// Encrypts numbers to a hash string.
        /// </summary>
        /// <param name="numbers">Numbers to encrypt.</param>
        /// <returns>Hash string.</returns>
        public string Encrypt(params int[] numbers)
        {
            if (numbers == null || numbers.Length == 0) return string.Empty;

            return numbers.Any(number => number < 0)
                ? string.Empty
                : encode(numbers);
        }

        /// <summary>
        /// Decrypt a hash string to numbers.
        /// </summary>
        /// <param name="hash">Hash string.</param>
        /// <returns>Decryped numbers.</returns>
        public int[] Decrypt(string hash)
        {
            return string.IsNullOrEmpty(hash)
                ? new int[0]
                : decode(hash);
        }

        private string encode(IList<int> numbers)
        {
            string ret;
            int numbersSize = numbers.Count;
            int numbersHashInt = 0;

            string alphabet = _alphabet;

            for (int i = 0; i < numbersSize; i++)
                numbersHashInt += (numbers[i] % (i + 100));

            string lottery = ret = alphabet.Substring(numbersHashInt % alphabet.Length, 1);
            for (int i = 0; i < numbersSize; i++)
            {
                int number = numbers[i];
                string buffer = lottery + _salt + alphabet;

                alphabet = consistentShuffle(alphabet, buffer.Substring(0, alphabet.Length));
                string last = hash(number, alphabet);

                ret += last;

                if (i + 1 >= numbersSize) continue;

                number %= (last[0] + i);
                int sepsIndex = number % _seps.Length;
                ret += _seps[sepsIndex];
            }

            if (ret.Length < _minHashLength)
            {
                int guardIndex = (numbersHashInt + ret[0]) % _guards.Length;
                char guard = _guards[guardIndex];

                ret = guard + ret;

                if (ret.Length < _minHashLength)
                {
                    guardIndex = (numbersHashInt + ret[2]) % _guards.Length;
                    guard = _guards[guardIndex];

                    ret += guard;
                }
            }

            int halfLength = alphabet.Length / 2;
            while (ret.Length < _minHashLength)
            {
                alphabet = consistentShuffle(alphabet, alphabet);
                ret = alphabet.Substring(halfLength) + ret + alphabet.Substring(0, halfLength);

                int excess = ret.Length - _minHashLength;
                if (excess > 0) ret = ret.Substring(excess / 2, _minHashLength);
            }

            return ret;
        }

        private int[] decode(string hash)
        {
            char[] seps = _seps.ToCharArray();

            string alphabet = _alphabet;
            var ret = new List<int>();
            int i = 0;
            string[] hashArray = hash.Split(_guards.ToCharArray());

            if (hashArray.Length == 3 || hashArray.Length == 2) i = 1;

            string hashBreakdown = hashArray[i];
            if (hashBreakdown.Length != 0)
            {
                char lottery = hashBreakdown[0];
                hashBreakdown = hashBreakdown.Substring(1);

                hashBreakdown = string.Join(" ", hashBreakdown.Split(seps));
                hashArray = hashBreakdown.Split(' ');

                for (i = 0; i < hashArray.Length; i++)
                {
                    string subHash = hashArray[i];
                    string buffer = lottery + _salt + alphabet;

                    alphabet = consistentShuffle(alphabet, buffer.Substring(0, alphabet.Length));
                    ret.Add(unhash(subHash, alphabet));
                }

                if (encode(ret) != hash) ret.Clear();
            }

            return ret.ToArray();
        }

        private static int unhash(string input, string alphabet)
        {
            int number = 0;
            for (int i = 0; i < input.Length; i++)
            {
                int pos = alphabet.IndexOf(input[i]);
                number += pos * (int)Math.Pow(alphabet.Length, input.Length - i - 1);
            }
            return number;
        }
    }
}
